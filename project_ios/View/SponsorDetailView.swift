//
//  SponsorDetailView.swift
//  project_ios
//
//  Created by Thomas GUILLAUME on 07/04/2021.
//

import SwiftUI

struct SponsorDetailView: View {
    
    var sponsor: Sponsor = Sponsor()
    
    @ObservedObject var viewModel = SponsorViewModel()

    
    var body: some View {
        GeometryReader { g in
            ScrollView {
                VStack(alignment: .leading, spacing: 40) {
                    Text(sponsor.fields.company ?? "Unknow Company")
                        .foregroundColor(.black)
                        .font(.system(size: 25, weight: .semibold))
                        .multilineTextAlignment(.leading)
                    Text("Contacts")
                        .foregroundColor(.black)
                        .font(.system(size: 18, weight: .semibold))
                        .multilineTextAlignment(.center)
                    Text("Coming soon 😊")
                        .foregroundColor(.black)
                        .multilineTextAlignment(.center)
                }
                .padding()
                .padding(.top, 20)
            }
        }
    }
}

struct SponsorDetailView_Previews: PreviewProvider {
    static var previews: some View {
        SponsorDetailView()
    }
}
