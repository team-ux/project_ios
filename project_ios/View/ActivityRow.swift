//
//  ActivityRow.swift
//  project_ios
//
//  Created by Thomas GUILLAUME on 06/04/2021.
//

import SwiftUI

struct ActivityRow: View {
    
    var activity: Activity = Activity()
    @State var showView = false
    
    var body: some View {
        VStack{

            HStack(alignment:.center){
                Image(systemName: getIcon(type: activity.fields.type)).resizable()
                    .frame(width: 50, height: 54)
                    .padding(.trailing,10)
                    .foregroundColor(.white)
                VStack(alignment:.leading){
                    Text(activity.fields.activity).font(.system(size: 20,weight : .bold, design:.rounded)).foregroundColor(.white)
                    Divider().hidden()
                    Text(activity.fields.startDate).font(.system(size: 16,weight : .bold, design:.rounded)).multilineTextAlignment(.center).foregroundColor(.white)
                    Text(activity.fields.startTime + " : " + activity.fields.endTime).font(.system(size: 16,weight : .bold, design:.rounded)).multilineTextAlignment(.center).foregroundColor(.white)
//                    Text(ListData.Location).font(.system(size: 14,weight : .bold, design:.rounded)).foregroundColor(.white)
                }
                Spacer()
            }
            Rectangle().fill(Color.white).frame( height: 2).padding(.vertical)


            HStack{
                NavigationLink(destination: DetailActivityView(id: activity.id), isActive: $showView) {
                    Text("View info")
                        .foregroundColor(.white)
                        .fontWeight(.regular)
                        .padding(6)
                        .overlay(RoundedRectangle(cornerRadius:20)
                                    .stroke(Color.white,lineWidth:2))
                }
            }
        }.padding(20)
        .background(getColor(type: activity.fields.type))
        .cornerRadius(20)
            .edgesIgnoringSafeArea(.bottom)

    }
    
    func getIcon(type: String) -> String {
        switch type {
        case "Meal":
            return "house"
        case "Keynote":
            return "note.text"
        case "Panel":
            return "network"
        case "Workshop":
            return "studentdesk"
        case "Breakout session":
            return "bubble.left.and.bubble.right.fill"
        default:
            return "archivebox"
        }
    }
    
    func getColor(type: String) -> Color {
        switch type {
        case "Meal":
            return Color.yellow
        case "Keynote":
            return Color.orange
        case "Panel":
            return Color.blue
        case "Workshop":
            return Color.pink
        case "Breakout session":
            return Color.purple
        default:
            return Color.black
        }
    }
}

struct ActivityRow_Previews: PreviewProvider {
    static var previews: some View {
        ActivityRow()
    }
}

//var List = [
//    ListData(id: 0, Title: "Breakfast",Icon: "calendar", Color: Color.yellow,Location: "Grand Ballroom"),
//    ListData(id: 1, Title: "Opening remark",Icon: "lock.rotation.open", Color: Color.orange,Location: "Grand Ballroom"),
//    ListData(id: 2, Title: "Technology in the household",Icon: "studentdesk", Color: Color.blue,Location: "Grand Ballroom"),
//    ListData(id: 3, Title: "Workshop for security",Icon: "hammer.fill", Color: Color.pink,Location: "Grand Ballroom"),
//    ListData(id: 4, Title: "Breakout session",Icon: "bubble.left.and.bubble.right.fill", Color: Color.purple,Location: "Grand Ballroom"),
//    ListData(id: 5, Title: "happy hour networking",Icon: "network", Color: Color.green,Location: "Grand Ballroom")]
