//
//  AttendeePage.swift
//  project_ios
//
//  Created by user188232 on 3/31/21.
//

import Foundation

import SwiftUI

struct SpeakerRow: View {
    var speaker: Speaker

    var body: some View {
        VStack{
            HStack {
                Image(systemName: "person")
                VStack(alignment: .leading) {
                    Text(speaker.fields.name!)
                    Text(speaker.fields.email!)
                        .font(.subheadline)
                        .italic()
                }
                Spacer()
                if(speaker.fields.type! == "Speaker"){
                    Image(systemName: "speaker.wave.3")
                        .padding()
                }
                if(speaker.fields.type! == "Attendee"){
                    Image(systemName: "person.circle.fill")
                        .padding()
                }
            }
        }
    }
}

    


struct SpeakersView: View {
    
    @ObservedObject var viewModel = SpeakerViewModel()
    
    @State private var attendeesSwitch: Bool = true
    @State private var speakerSwitch: Bool = true
        
        var body: some View {
            NavigationView {
                VStack{
                    HStack{
                        VStack{
                            HStack{
                                Text("Attendees")
                                Image(systemName: "person.circle.fill")
                            }
                            Toggle("",isOn: $attendeesSwitch).labelsHidden()
                        }
                        .padding()
                        VStack{
                            HStack{
                                Text("Speaker")
                                Image(systemName: "speaker.wave.3")
                            }
                            Toggle("",isOn: $speakerSwitch).labelsHidden()
                        }
                        .padding()
                    }
                    HStack{
                        SwiftUI.List{
                            if(attendeesSwitch && speakerSwitch){
                                Text("Attendees & Speakers")
                                    .font(.system(size: 20, weight: .bold, design: .default))
                                VStack(spacing: 20) {
                                    ForEach(viewModel.speakersAndAttendees.records){speaker in
                                        SpeakerRow(speaker: speaker)
                                    }
                                }.onAppear {
                                    viewModel.fetchSpeakersAndAttendees()
                                }
                                .padding(.top, 20)
                            }
                            if(speakerSwitch && !attendeesSwitch){
                                Text("Speaker")
                                    .font(.system(size: 20, weight: .bold, design: .default))
                                VStack(spacing: 20) {
                                    ForEach(viewModel.speakers.records){speaker in
                                        SpeakerRow(speaker: speaker)
                                    }
                                }.onAppear {
                                    viewModel.fetchSpeakers()
                                }
                                .padding(.top, 20)
                            }
                            if(attendeesSwitch && !speakerSwitch){
                                Text("Attendees")
                                    .font(.system(size: 20, weight: .bold, design: .default))
                                VStack(spacing: 20) {
                                    ForEach(viewModel.attendees.records){speaker in
                                        SpeakerRow(speaker: speaker)
                                    }
                                }.onAppear {
                                    viewModel.fetchAttendee()
                                }
                                .padding(.top, 20)
                            }
                        }
                    }
                }.navigationBarTitle("")
                .navigationBarHidden(true)
            }
            .navigationBarTitle("Attendees & Speakers")
            .navigationViewStyle(StackNavigationViewStyle())

        }
}

struct AttendeePage_Preview: PreviewProvider {
    static var previews: some View {
            SpeakersView()
    }
}
