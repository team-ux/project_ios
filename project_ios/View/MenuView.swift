import SwiftUI

struct MenuView: View {
    var body: some View {
        VStack(alignment: .leading){
            HStack{
                NavigationLink(
                    destination: SpeakersView(),
                    label: {
                        Image(systemName: "speaker")
                            .foregroundColor(.white)
                            .imageScale(.large)
                        Text("Attendee/Speaker")
                            .foregroundColor(.white)
                            .font(.subheadline).bold()
                    })
            }
            .padding(.top,100)
            
            HStack{
                NavigationLink(
                    destination: SponsorsView(),
                    label: {
                        Image(systemName: "hand.wave")
                            .foregroundColor(.white)
                            .imageScale(.large)
                        Text("Sponsor")
                            .foregroundColor(.white)
                            .font(.subheadline).bold()
                    })
            }
            .padding(.top,30)
            Spacer()
        }
            .padding()
            .frame(maxWidth: .infinity, alignment: .leading)
        .background(Color(red: 242/255, green: 143/255, blue: 59/255))
        .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    }
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView()
    }
}
