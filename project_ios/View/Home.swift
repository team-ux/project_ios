//
//  SponsorList.swift
//  project-ios
//
//  Created by user188889 on 3/24/21.
//

import SwiftUI

struct Home: View {
    @ObservedObject var viewModel = ActivityViewModel()
    init() {
        viewModel.fetchActivities()
    }
    
    @State var index=0
    @State var showMenu = false
    var body: some View {
        VStack {
            Text("AirTable Events")
               .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
               .fontWeight(.bold)
               .padding(.top, 20)
            SwiftUI.List {
                ForEach(viewModel.activities.records.sorted(by: { (Activity, Activity2) -> Bool in
                    Activity.fields.startDate < Activity2.fields.startDate
                })) { activity in
                    ActivityRow(activity: activity)
              }
            }
        }.navigationBarTitle("Activities")
    }
}




struct Home_Previews: PreviewProvider {
    static var previews: some View {
        Group {
        
            Home()
    
        }
    }
}
