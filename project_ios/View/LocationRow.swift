//
//  LocationRow.swift
//  project_ios
//
//  Created by Thomas GUILLAUME on 04/04/2021.
//

import SwiftUI

struct LocationRow: View {
    
    var location: Location = Location()
    
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            Text("\(location.fields.spaceName ?? "")").font(.system(size: 20, weight: .semibold))
            HStack {
                Image(systemName: "building").resizable()
                    .frame(width: 16, height: 16)
                    .padding(.trailing,10)
                    .foregroundColor(.black)
                Text("\(location.fields.buildingLocation ?? "")")
            }
            VStack(alignment: .leading) {
                Text("Description : ").font(.system(size: 14, weight: .semibold))
                Text("\(location.fields.description ?? "")")
            }
        }.padding()
        .overlay(
            RoundedRectangle(cornerRadius: 8)
                .stroke(Color.black, lineWidth: 4)
        )
    }
}

struct LocationRow_Previews: PreviewProvider {
    static var previews: some View {
        LocationRow()
    }
}
