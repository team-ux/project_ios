//
//  DetailActivity.swift
//  project-ios
//
//  Created by user188232 on 3/24/21.
//

import SwiftUI

struct DetailActivityView: View {
    

    @ObservedObject var viewModel = ActivityViewModel()
        
    var id: String = ""
    
    init(id: String) {
        self.id = id
    }
    
    init() {
    }
    
    var body: some View {
        
        GeometryReader { g in
            ScrollView {
                VStack{
                    Text(viewModel.activity.fields.activity)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading)
                        .font(.title)
                    Text(viewModel.activity.fields.type)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading)
                    Text(viewModel.activity.fields.startDate)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading)
                    HStack(alignment: .center){
                        Image(systemName: "clock").resizable()
                            .frame(width: 16, height: 16)
                            .padding(.trailing,10)
                            .foregroundColor(.black)
                        Text(viewModel.activity.fields.startTime)
                            .padding()
                        Text("-")
                            .padding()
                        Text(viewModel.activity.fields.endTime)
                            .padding()
                    }
                }
                .background(Color.gray.opacity(0.1))
                .cornerRadius(8)
                .padding(.leading)
                .padding(.trailing)
                
                if (viewModel.speakers.count > 0) {
                    // speakers
                    VStack{
                        Text("Speakers")
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.leading)
                            .font(.title)
                    }
                    
                    List {
                        ForEach(viewModel.speakers) { speaker in
                            VStack{
                                HStack {
                                    Image(systemName: "person.icloud")
                                    VStack(alignment: .leading) {
                                        Text(speaker.fields.name!)
                                        Text(speaker.fields.email!)
                                            .font(.subheadline)
                                            .italic()
                                            
                                        }
                                    .padding(.leading)
                                    Spacer()
                                    }
                            }
                        }
                    }.frame(width: g.size.width, height: 100, alignment: .center)
                }
                
                if (viewModel.topics.count > 0) {
                    // topics
                    VStack{
                        Text("Topics")
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.leading)
                            .font(.title)
                    }
                    
                    List {
                        ForEach(viewModel.topics) { topic in
                            VStack{
                                HStack {
                                    Image(systemName: "checkerboard.rectangle")
                                    VStack(alignment: .leading) {
                                        Text(topic.fields.topic)
                                        }
                                    .padding(.leading)
                                    Spacer()
                                    }
                            }
                        }
                    }.frame(width: g.size.width, height: 100, alignment: .center)
                }

                if (viewModel.locations.count > 0) {
                    // location
                    VStack{
                        Text("Locations")
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.leading)
                            .font(.title)
                    }
                    
                    List {
                        ForEach(viewModel.locations) { location in
                            LocationRow(location: location)
                        }
                    }.frame(width: g.size.width, height: g.size.height, alignment: .center)
                }
            }
        }.onAppear {
            self.fetchData()
        }.padding(.top, 20)
    }
    
    func fetchData() {
        self.viewModel.fetchActivityDetail(id: id)
    }
}

struct DetailActivity_Previews: PreviewProvider {
    static var previews: some View {
        DetailActivityView()
    }
}
