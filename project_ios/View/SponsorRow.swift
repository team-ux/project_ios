//
//  SponsorRow.swift
//  project_ios
//
//  Created by Thomas GUILLAUME on 06/04/2021.
//

import SwiftUI

struct SponsorRow: View {
    
    var sponsor: Sponsor
    
    var body: some View {
        VStack(spacing: 8) {
            GroupBox(label: Text(sponsor.fields.company ?? "Unknow Company").foregroundColor(.black).font(.system(size: 25, weight: .semibold))) {
            }.groupBoxStyle(HealthGroupBoxStyle(color: .red, destination: SponsorDetailView(sponsor: sponsor), prev: sponsor.fields.previousSponsor ?? false))
        }.padding()
    }
}

struct SponsorRow_Previews: PreviewProvider {
    static var previews: some View {
        SponsorRow(sponsor: Sponsor())
    }
}

struct HealthGroupBoxStyle<V: View>: GroupBoxStyle {
    var color: Color
    var destination: V
    var date: Date?
    var prev : Bool
    

    @ScaledMetric var size: CGFloat = 1
    
    func makeBody(configuration: Configuration) -> some View {
        NavigationLink(destination: destination) {
            GroupBox(label: HStack {
                configuration.label.foregroundColor(color)
                Spacer()
                
                if (prev) {
                
                        Image(systemName: "heart.fill").foregroundColor(.red).padding(.top, 10)
                }
            }) {
                configuration.content
            }
        }
    }
}
