//
//  Sponsors.swift
//  project_ios
//
//  Created by user188889 on 3/31/21.
//

import SwiftUI

struct SponsorsView: View {
    
    @ObservedObject var viewModel = SponsorViewModel()
    @State private var numbers = [1,2,3,4,5,6,7,8,9]

    var body: some View {
        VStack {
            List {
                ForEach(viewModel.sponsors.records.sorted(by: { (a, b) -> Bool in
                    if(a.fields.previousSponsor != nil) {
                        return a.fields.previousSponsor!
                    } else {
                        return false
                    }
                })) { sponsor in
                    SponsorRow(sponsor: sponsor)
                }
            }.onAppear(){
                viewModel.fetchSponsors()
            }
           }
        .navigationBarTitle("Sponsors")
    }
}

struct Sponsors_Previews: PreviewProvider {
    static var previews: some View {
        SponsorsView()
    }
}
