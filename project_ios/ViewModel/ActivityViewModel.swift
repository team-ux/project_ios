//
//  ActivityViewModel.swift
//  project_ios
//
//  Created by Thomas GUILLAUME on 31/03/2021.
//

import Combine
import SwiftUI

class ActivityViewModel: ObservableObject {
    
    public let speakerService = SpeakerService()
    public let activityService = ActivityService()

    
    private var taskDetailActivity: AnyCancellable?
    private var taskLocations: AnyCancellable?
    private var taskSpeakers: AnyCancellable?
    private var taskTopics: AnyCancellable?
    private var taskActivities: AnyCancellable?
    
    @Published var speakers: [Speaker] = []
    @Published var locations: [Location] = []
    @Published var topics: [Topic] = []
    @Published var activity: Activity = Activity()
    @Published var activities: ActivityRecords = ActivityRecords(records: [])

    /**
            Fetch actiivty detail from AirTable API
            Create a data task publisher to request data and bind with view model "activity"
     */
    func fetchActivityDetail(id: String) {
        
        let publisher = self.activityService.getActivity(id: id)
        
        // fetch activity info
        taskDetailActivity = publisher
            .receive(on: RunLoop.main)
            .assign(to: \ActivityViewModel.activity, on: self)
    
        // fetch locations
        taskLocations = publisher
            .flatMap { location in
                Publishers.Sequence(sequence: location.fields.location.map { self.activityService.getLocation(id: $0)})
                .flatMap { $0 }
                .collect()
        }.eraseToAnyPublisher()
        .assign(to: \ActivityViewModel.locations, on: self)
        
        // fetch speakers
        taskSpeakers = publisher
            .flatMap { activity in
                Publishers.Sequence(sequence: activity.fields.speakers
                .map { self.speakerService.getSpeakerbyId(id: $0)})
                .flatMap { $0 }
                .collect()
        }
        .eraseToAnyPublisher()
        .assign(to: \ActivityViewModel.speakers, on: self)
        
        // fetch topics
        taskTopics = publisher
            .flatMap { topics in
                Publishers.Sequence(sequence: topics.fields.topics.map { self.activityService.getTopic(id: $0)})
                .flatMap { $0 }
                .collect()
        }.eraseToAnyPublisher()
        .assign(to: \ActivityViewModel.topics, on: self)
        

    }
    
    /**
            Fetch all activies from AirTable API
            Create a data task publisher to request data and bind with view model "activities"
     */
    func fetchActivities() {
        taskActivities = self.activityService.getActivities()
            .receive(on: RunLoop.main)
            .assign(to: \ActivityViewModel.activities, on: self)

    }
    
    
}
