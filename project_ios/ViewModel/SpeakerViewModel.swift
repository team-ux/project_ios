//
//  SpeakerViewModel.swift
//  project_ios
//
//  Created by Thomas GUILLAUME on 04/04/2021.
//

import Combine
import SwiftUI

class SpeakerViewModel: ObservableObject {
    
    
    private var speakerService: SpeakerService = SpeakerService()
    
    private var taskSpeakers: AnyCancellable?
    private var taskAttendee: AnyCancellable?
    private var taskAll: AnyCancellable?

    @Published var speakersAndAttendees: SpeakerRecords = SpeakerRecords()
    @Published var speakers: SpeakerRecords = SpeakerRecords()
    @Published var attendees: SpeakerRecords = SpeakerRecords()

    /**
            Fetch all speakers and attendee from AirTable API
            Create a data task publisher to request data and bind with view model "speakers"
     */
    func fetchSpeakersAndAttendees() {
        taskAll = self.speakerService.getAllPeople()
            .receive(on: RunLoop.main)
            .assign(to: \SpeakerViewModel.speakersAndAttendees, on: self)
    }
    
    /**
            Fetch all speakers from AirTable API
            Create a data task publisher to request data and bind with view model "speakers"
     */
    func fetchSpeakers() {
        taskSpeakers = self.speakerService.getAttendeesOrSpeakers(type: "Speaker")
            .receive(on: RunLoop.main)
            .assign(to: \SpeakerViewModel.speakers, on: self)
    }
    
    /**
            Fetch all attendees from AirTable API
            Create a data task publisher to request data and bind with view model "attendees"
     */
    func fetchAttendee() {
        taskAttendee = self.speakerService.getAttendeesOrSpeakers(type: "Attendee")
            .receive(on: RunLoop.main)
            .assign(to: \SpeakerViewModel.attendees, on: self)
    }
    
    
}
