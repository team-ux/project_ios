//
//  SponsorViewModel.swift
//  project_ios
//
//  Created by Thomas GUILLAUME on 04/04/2021.
//

import Combine
import SwiftUI

class SponsorViewModel: ObservableObject {

    public let requestService = RequestService()
    public let speakerService = SpeakerService()
    public let sponsorService = SponsorService()

    public let decoder = JSONDecoder()
    
    private var taskSponsors: AnyCancellable?
    private var taskContacts: AnyCancellable?

    @Published var sponsors: SponsorRecords = SponsorRecords()
    @Published var speakers: [Speaker] = []

    
    /**
            Fetch all sponsors from AirTable API
            Create a data task publisher to request data and bind with view model "sponsors"
     */
    func fetchSponsors() {
        taskSponsors = self.sponsorService.getSponsors()
            .receive(on: RunLoop.main)
            .assign(to: \SponsorViewModel.sponsors, on: self)
//            .sink(receiveValue: { (SponsorRecords) in
//                print(SponsorRecords.self)
//            })
    }
    
    
//    // publisher to get all sponsors
//    func fetchContacts(id: String) -> AnyPublisher<[Speaker], Never> {
//        
//        let publisher = self.sponsorService.getSponsorById(id: id)
//        // fetch speakers
//        taskContacts = publisher
//            .flatMap { sponsor in
//                Publishers.Sequence(sequence: sponsor.fields.contacts
//                .map { self.speakerService.getSpeakerbyId(id: $0)})
//                .flatMap { $0 }
//                .collect()
//        }
//        .eraseToAnyPublisher()
//        .assign(to: \ActivityViewModel.speakers, on: self)
//    }
    
   
    
    
}
