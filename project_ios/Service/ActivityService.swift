//
//  ActivityService.swift
//  project_ios
//
//  Created by Thomas GUILLAUME on 07/04/2021.
//

import Combine
import SwiftUI

class ActivityService {
    
    private let requestService: RequestService = RequestService()
    private let decoder: JSONDecoder = JSONDecoder()
    
    // Publisher to fetch all activity
    func getActivities() -> AnyPublisher<ActivityRecords, Never> {
        let requestUrlActivity = requestService.createRequest(urlStr: "Schedule", type: "GET")
        return URLSession.shared.dataTaskPublisher(for: requestUrlActivity)
            .tryMap() { element -> Data in
                    guard let httpResponse = element.response as? HTTPURLResponse,
                        httpResponse.statusCode == 200 else {
                            throw URLError(.badServerResponse)
                        }
                    return element.data
                    }
            .decode(type: ActivityRecords.self, decoder: self.decoder)
            .replaceError(with: ActivityRecords(records: []))
            .eraseToAnyPublisher()
    }
    
    // Publisher to fetch an Activity by ID
    func getActivity(id: String) -> AnyPublisher<Activity, Never> {
        let requestUrlActivity = requestService.createRequest(urlStr: "Schedule/" + id, type: "GET")
        return URLSession.shared.dataTaskPublisher(for: requestUrlActivity)
            .receive(on: RunLoop.main)
            .tryMap() { element -> Data in
                    guard let httpResponse = element.response as? HTTPURLResponse,
                        httpResponse.statusCode == 200 else {
                            throw URLError(.badServerResponse)
                        }
                    return element.data
                    }
            .decode(type: Activity.self, decoder: JSONDecoder())
            .replaceError(with: Activity())
            .eraseToAnyPublisher()
    }
    
    // Publisher to fetch a Location by ID
    public func getLocation(id: String) -> AnyPublisher<Location, Never> {
        let requestUrlActivity = requestService.createRequest(urlStr: "Event%20locations/" + id, type: "GET")
        return URLSession.shared.dataTaskPublisher(for: requestUrlActivity)
            .receive(on: RunLoop.main)
            .tryMap() { element -> Data in
                    guard let httpResponse = element.response as? HTTPURLResponse,
                        httpResponse.statusCode == 200 else {
                            throw URLError(.badServerResponse)
                        }
                    return element.data
                    }
            .decode(type: Location.self, decoder: JSONDecoder())
            .replaceError(with: Location())
            .eraseToAnyPublisher()
    }
    
    // Publisher to fetch a Topic by ID
    public func getTopic(id: String) -> AnyPublisher<Topic, Never> {
        let requestUrlActivity = requestService.createRequest(urlStr: "Topics%20%26%20themes/" + id, type: "GET")
        return URLSession.shared.dataTaskPublisher(for: requestUrlActivity)
            .receive(on: RunLoop.main)
            .tryMap() { element -> Data in
                    guard let httpResponse = element.response as? HTTPURLResponse,
                        httpResponse.statusCode == 200 else {
                            throw URLError(.badServerResponse)
                        }
                    return element.data
                    }
            .decode(type: Topic.self, decoder: JSONDecoder())
            .replaceError(with: Topic())
            .eraseToAnyPublisher()
    }
}
