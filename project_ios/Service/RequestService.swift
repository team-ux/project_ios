//
//  RequestService.swift
//  project-ios
//
//  Created by Thomas GUILLAUME on 24/03/2021.
//

import Combine
import SwiftUI

class RequestService {

    // https://api.airtable.com/v0/appXKn0DvuHuLw4DV/Schedule/rec38jO5T05cqdJDx?api_key=keyDRNl4fQsKMjAz7
    
    static let BASE_URL: String = "https://api.airtable.com/v0/appXKn0DvuHuLw4DV"
    static let API_KEY: String = "keyDRNl4fQsKMjAz7"
    
    func createRequest(urlStr: String = "", type: String, parameters: [String: String]? = nil) -> URLRequest {
        
        var paramURL = ""
        if parameters != nil {
            paramURL += createParametersURL(parameters: parameters.unsafelyUnwrapped)
        }
        let url = URL(string: (RequestService.BASE_URL + "/" + urlStr + paramURL))!
        var request = URLRequest(url: url)
        request.httpMethod = type
        request.timeoutInterval = 100
        addAuthorizationHeader(request: &request)
        return request
    }
    
    func createParametersURL(parameters: [String: String]) -> String {
        var url: String = ""
        // sort param dict by key
        let parametersSorted: [(key: String, value: String)] = parameters.sorted {
            return $0.key < $1.key
        }
        for (index, param) in parametersSorted.enumerated() {
            url.append((index == 0 ? "?" : "&") + param.key + "=" + param.value)
        }
        return url;
    }
    
    func addAuthorizationHeader(request: inout URLRequest) {
        request.setValue("Bearer \(RequestService.API_KEY)", forHTTPHeaderField: "Authorization")
    }
}
