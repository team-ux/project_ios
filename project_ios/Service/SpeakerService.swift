//
//  SpeakerService.swift
//  project_ios
//
//  Created by Thomas GUILLAUME on 07/04/2021.
//

import SwiftUI
import Combine

class SpeakerService {
    
    private let requestService = RequestService()
    private let decoder: JSONDecoder = JSONDecoder()
    
    // publisher to get attendees AND speakers
    func getAllPeople() -> AnyPublisher<SpeakerRecords, Never> {
        let requestURL = requestService.createRequest(urlStr: "Speakers%20%26%20attendees", type: "GET")
        return URLSession.shared.dataTaskPublisher(for: requestURL)
            .tryMap() { element -> Data in
                    guard let httpResponse = element.response as? HTTPURLResponse,
                        httpResponse.statusCode == 200 else {
                            throw URLError(.badServerResponse)
                        }
                    return element.data
                    }
            .decode(type: SpeakerRecords.self, decoder: self.decoder)
            .replaceError(with: SpeakerRecords(records: []))
            .eraseToAnyPublisher()
    }

    
    // publisher to get attendees OR speakers
    func getAttendeesOrSpeakers(type: String) -> AnyPublisher<SpeakerRecords, Never> {
        let param = ["filterByFormula": addTypeParam(type: type)]
        let requestURL = requestService.createRequest(urlStr: "Speakers%20%26%20attendees", type: "GET", parameters: param)
        return URLSession.shared.dataTaskPublisher(for: requestURL)
            .tryMap() { element -> Data in
                    guard let httpResponse = element.response as? HTTPURLResponse,
                        httpResponse.statusCode == 200 else {
                            throw URLError(.badServerResponse)
                        }
                    return element.data
                    }
            .decode(type: SpeakerRecords.self, decoder: self.decoder)
            .replaceError(with: SpeakerRecords())
            .eraseToAnyPublisher()
    }

    private func addTypeParam(type: String) -> String {
        return "%7BType%7D%20%3D%20%27" + type + "%27"
    }
    
    // Publisher to fetch a Location by ID
    public func getSpeakerbyId(id: String) -> AnyPublisher<Speaker, Never> {
        let requestUrlActivity = requestService.createRequest(urlStr: "Speakers%20%26%20attendees/" + id, type: "GET")
        return URLSession.shared.dataTaskPublisher(for: requestUrlActivity)
            .receive(on: RunLoop.main)
            .tryMap() { element -> Data in
                    guard let httpResponse = element.response as? HTTPURLResponse,
                        httpResponse.statusCode == 200 else {
                            throw URLError(.badServerResponse)
                        }
                    return element.data
                    }
            .decode(type: Speaker.self, decoder: JSONDecoder())
            .replaceError(with: Speaker())
            .eraseToAnyPublisher()
    }

    
}
