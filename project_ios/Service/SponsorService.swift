//
//  SponsorService.swift
//  project_ios
//
//  Created by Thomas GUILLAUME on 07/04/2021.
//

import Combine
import SwiftUI


class SponsorService {
    
    private let decoder = JSONDecoder()
    private let requestService = RequestService()
    
    // publisher to get all sponsors
    func getSponsors() -> AnyPublisher<SponsorRecords, Never> {
        let requestURL = requestService.createRequest(urlStr: "Sponsors", type: "GET")
        print(requestURL)
        return URLSession.shared.dataTaskPublisher(for: requestURL)
            .map { $0.data }
            .decode(type: SponsorRecords.self, decoder: self.decoder)
            .replaceError(with: SponsorRecords(records: []))
            .eraseToAnyPublisher()
    }
    
    // publisher to get sponsor by an ID
    func getSponsorById(id: String) -> AnyPublisher<Sponsor, Never> {
        let requestURL = requestService.createRequest(urlStr: "Sponsors/" + id, type: "GET")
        print(requestURL)
        return URLSession.shared.dataTaskPublisher(for: requestURL)
            .map { $0.data }
            .decode(type: Sponsor.self, decoder: self.decoder)
            .replaceError(with: Sponsor())
            .eraseToAnyPublisher()
    }
}
