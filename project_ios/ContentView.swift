//
//  ContentView.swift
//  project-ios
//
//  Created by Thomas GUILLAUME on 24/03/2021.
//

import SwiftUI

struct ContentView: View {
    @State var showMenu = false
  
    var body: some View {
    
        let drag = DragGesture()
            .onEnded {
                if $0.translation.width < -100{
                    withAnimation {
                        self.showMenu = false
                    }
                }
            }
    
        
        return NavigationView{
            GeometryReader{ geometry in
            ZStack(alignment: .leading){
                MainView(showMenu: self.$showMenu)
                    .frame(width: geometry.size.width, height: geometry.size.height)
                    .offset(x: self.showMenu ? geometry.size.width/2 : 0)
                    .disabled(self.showMenu ? true : false)
                    .onAppear(){
                        self.showMenu = false
                    }

                if self.showMenu{
                    MenuView().frame(width: geometry.size.width/2)
                        .transition(.move(edge: .leading))
                }
                
            }
            .gesture(drag)
        }
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarTitle(Text("Home"))
            
            
           
            .navigationBarItems(leading: (
                Button(action: {
                        withAnimation {
                            self.showMenu.toggle()
                                            
                            }
                                        
                        }) {
                        Image(systemName: "line.horizontal.3")
                            .resizable()
                            .frame(width:22, height: 18)
                            .foregroundColor(Color.black)
                        
                    
                    }
            ))
            
        }.navigationViewStyle(StackNavigationViewStyle())
       
    
    }
}
struct MainView: View{
    @Binding var showMenu: Bool
    var body: some View{
        Home()
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
        
    }
}
