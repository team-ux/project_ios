//
//  Location.swift
//  project-ios
//
//  Created by user188799 on 3/24/21.
//

import Foundation

struct Location: Decodable, Identifiable, Equatable {
    
    var id: String = "";
    var fields: Fields = Fields();
    
    struct Fields: Decodable, Equatable {
        var spaceName: String? = "";
        var buildingLocation: String? = "";
        var photos: [Photo]? = [];
        var description: String? = "";
        var maxCapacity: Int? = 0;
        var scheduledEvents: [String]? = [];
        
        enum CodingKeys: String, CodingKey {
            case spaceName = "Space name"
            case description = "Description"
            case photos = "Photo(s)"
            case buildingLocation = "Building location"
            case scheduledEvents = "Scheduled events"
            case maxCapacity = "Max capacity"
        }
    }
    
    struct Photo: Codable, Equatable {
        var id: String = "";
        var url: String = "";
        var filename: String = "";
        var size: Int = 0;
        var type: String = "";
    }
}

struct LocationRecords: Decodable {
    var records: [Location] = []
}
