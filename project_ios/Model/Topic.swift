//
//  Topic.swift
//  project_ios
//
//  Created by Thomas GUILLAUME on 05/04/2021.
//

import Foundation

struct Topic: Decodable, Identifiable, Equatable {
    
    var id: String = "";
    var fields: Fields = Fields()
    
    struct Fields: Decodable, Equatable {
        var topic: String = "";
        var count: Int = 0;
        
        enum CodingKeys: String, CodingKey {
            case topic = "Topic / theme"
            case count = "Count"
        }
    }
    
}
