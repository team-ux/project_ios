//
//  Speaker.swift
//  project-ios
//
//  Created by user188799 on 3/24/21.
//

import Foundation

struct Speaker: Decodable, Identifiable, Equatable {
    
    var id: String = "";
    var fields: Fields = Fields()
    
    struct Fields: Decodable, Equatable {
        var role: String? = "";
        var company: [String]? = [];
        var status: String? = "";
        var email: String? = "";
        var type: String? = "";
        var speakingAt: [String]? = [];
        var name: String? = "";
        var phone: String? = "";
        
        enum CodingKeys: String, CodingKey {
            case role = "Role"
            case company = "Company"
            case status = "Status"
            case email = "Email"
            case type = "Type"
            case speakingAt = "Speaking at"
            case name = "Name"
            case phone = "Phone"
        }
        
    }
}
struct SpeakerRecords: Decodable {
    
    var records: [Speaker] = []
    
}
