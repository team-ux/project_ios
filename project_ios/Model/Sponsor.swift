//
//  Sponsor.swift
//  project_ios
//
//  Created by Thomas GUILLAUME on 04/04/2021.
//

import Foundation

struct Sponsor: Decodable, Identifiable {
    
    var id: String = "";
    
    var fields: Fields = Fields()
    
    struct Fields: Decodable {
        var company: String? = "";
        var contacts: [String]? = [];
        var previousSponsor: Bool? = false;
        var notes: String? = ""
        var sponsoredAmount: Int? = 0
        var status: String? = ""
        
        enum CodingKeys: String, CodingKey {
            case company = "Company"
            case contacts = "Contact(s)"
            case previousSponsor = "Previous sponsor"
            case notes = "Notes"
            case sponsoredAmount = "Sponsored amount"
            case status = "Status"
        }
    }
    
}


struct SponsorRecords: Decodable {
    var records: [Sponsor] = []
    
    public var totalReceivedPleged: Int {
        return records.filter { (Sponsor) -> Bool in
            return Sponsor.fields.status == "Received pledged $"
        }.map { (Sponsor) -> Int in
            return (Sponsor.fields.sponsoredAmount ?? 0)
        }.reduce(0, { x, y in
            x + y
        })
    }
    
    public var totalPleged: Int {
        return records.filter { (Sponsor) -> Bool in
            return Sponsor.fields.status == "Pledged $"
        }.map { (Sponsor) -> Int in
            return (Sponsor.fields.sponsoredAmount ?? 0)
        }.reduce(0, { x, y in
            x + y
        })
    }
    
}
