//
//  Activity.swift
//  project-ios
//
//  Created by user188799 on 3/24/21.
//

import Foundation
struct Activity: Decodable, Identifiable {
    var id: String;
    
    struct Fields: Decodable {
                
        var end: String = "";
        var start: String = "";
        var activity: String = "";
        var type: String = "";
        var location: [String] = [];
        var notes: String? = "";
        
        @DecodableDefault.EmptyList var speakers: [String];
        @DecodableDefault.EmptyList var topics: [String];

        
        enum CodingKeys: String, CodingKey {
            case end = "End"
            case start = "Start"
            case activity = "Activity"
            case type = "Type"
            case location = "Location"
            case notes = "Notes"
            case speakers = "Speaker(s)"
            case topics = "Topic / theme"

        }
        
        public var endTime: String {
            return formatTime(str: end)
        }
        
        public var startTime: String {
           return formatTime(str: start)
        }
        
        public var endDate: String {
            return formatDate(str: end)
        }
        
        public var startDate: String {
           return formatDate(str: start)
        }
        
        private func formatTime(str: String) -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone(abbreviation: "GMT+0:00")! as TimeZone
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date = dateFormatter.date(from: str)
            var time = ""
            if date != nil {
                dateFormatter.dateFormat = "hh:mm a" // output format
                time = dateFormatter.string(from: date!)
            }
            return time
        }
        
        private func formatDate(str: String) -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone(abbreviation: "GMT+0:00")! as TimeZone
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date = dateFormatter.date(from: str)
            var time = ""
            if date != nil {
                dateFormatter.dateStyle = .medium
                dateFormatter.timeStyle = .none
                time = dateFormatter.string(from: date!)
            }
            return time
        }
        
    }
    
    init(id: String? = "", fields: Fields? = Fields()) {
        self.id = id!
        self.fields = fields!
    }
    
    var fields: Fields
}

struct ActivityRecords: Decodable {
    var records: [Activity]
}
