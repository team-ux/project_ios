# IOS Project 2020-2021 

## Subject 
This project allows you to navigate through an application and find useful information on a particular event such has the different activites, the steakers and attendees, the sponsors etc...

[**Airtable API**](https://airtable.com/tblAAOFAlGnrh26gL) 


## Prerequisites
You don't need anything in particular to build and run the project provided you have XCode 12.4 (at least, otherwise you may have trouble using the emulator) installed on your machine  

## Conception
This application is separated into different views:  
```
1. When you open the application you land on the home page which displays the main schedule of the first day (the list of activities). 
2. Each activity has a detail page.
3. There is a speaker and attendee list page that allows us to have information on them.
4. There is a page with a list of the sponsors of the event.
```

## Design Pattern
We used the MVVM Pattern
- Model
- View
- View Model

In terms of User Interface, we have used SwiftUI

### Models 

Our applications relies on 6 models : 
``` 
- Location
- Sponsor
- Speaker
- Activity
- Topic
- DecodableDefault
```

### Services
```
- RequestService  
- ActivityService
- SpeakerService
- SponsorService
```

### Views
```
- ActivityRow.swift
- DetailActivityView.swift
- Home.swift
- LocationRow.swift
- SpeakersView.swift
- SponsorDetailView.swift
- SponsorRow.swift
- SponsorView
```

### View Model
```
- SponsorViewModel
- SpeakerViewModel
- ActivityViewModel
```

### Models 
```
- Activity
- DecodableDefault
- Location
- Speaker 
- Sponsor
```

## Team  
**SE1 - Thomas Guillaume / Victoria Doe / Amine Ben Slama / François-Xavier Hillaire / Antonin Wieczorek**


