//
//  ActivityModelTest.swift
//  project_iosTests
//
//  Created by Thomas GUILLAUME on 07/04/2021.
//

import XCTest
@testable import project_ios

class ActivityModelTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testActivityInit() {
        let activity: Activity = Activity(id: "1", fields: Activity.Fields(end: "2019-11-15T17:50:00.000Z", start: "2019-11-15T16:50:00.000Z", activity: "Activity", type: "Breakfast", location: ["1"], notes: "Note"))
        XCTAssertEqual(activity.id, "1")
        XCTAssertEqual(activity.fields.end, "2019-11-15T17:50:00.000Z")
        XCTAssertEqual(activity.fields.start, "2019-11-15T16:50:00.000Z")
        XCTAssertEqual(activity.fields.activity, "Activity")
        XCTAssertEqual(activity.fields.type, "Breakfast")
        XCTAssertEqual(activity.fields.location, ["1"])
        XCTAssertEqual(activity.fields.notes, "Note")
        
        XCTAssertEqual(activity.fields.endTime, "05:50 PM")
        XCTAssertEqual(activity.fields.startTime, "04:50 PM")
        XCTAssertEqual(activity.fields.startDate, "Nov 15, 2019")
        XCTAssertEqual(activity.fields.endDate, "Nov 15, 2019")

    }
    
    func testActivityWrongInit() {
        let activity: Activity = Activity(id: "3", fields: Activity.Fields(end: "2000-11-15T17:50:00.000Z", start: "2100-11-15T16:50:00.000Z", activity: "ActivityFake", type: "BreakfastFake", location: ["4"], notes: "NoteFake"))
        XCTAssertNotEqual(activity.id, "1")
        XCTAssertNotEqual(activity.fields.end, "2019-11-15T17:50:00.000Z")
        XCTAssertNotEqual(activity.fields.start, "2019-11-15T16:50:00.000Z")
        XCTAssertNotEqual(activity.fields.activity, "Activity")
        XCTAssertNotEqual(activity.fields.type, "Breakfast")
        XCTAssertNotEqual(activity.fields.location, ["1"])
        XCTAssertNotEqual(activity.fields.notes, "Note")
        
        XCTAssertNotEqual(activity.fields.endTime, "05:50 AM")
        XCTAssertNotEqual(activity.fields.startTime, "04:50 AM")
        XCTAssertNotEqual(activity.fields.startDate, "Nov 16, 2019")
        XCTAssertNotEqual(activity.fields.endDate, "Nov 12, 2019")
    }

}
