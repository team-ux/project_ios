//
//  TopicModelTest.swift
//  project_iosTests
//
//  Created by Thomas GUILLAUME on 07/04/2021.
//

import XCTest
@testable import project_ios

class TopicModelTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTopicInit() {
        let topic: Topic = Topic(id: "1", fields: Topic.Fields(topic: "topic", count: 3))
        XCTAssertEqual(topic.id, "1")
        XCTAssertEqual(topic.fields.topic, "topic")
        XCTAssertEqual(topic.fields.count, 3)
    }
    
    func testTopicWrongInit() {
        let topic: Topic = Topic(id: "1", fields: Topic.Fields(topic: "topic", count: 3))
        XCTAssertNotEqual(topic.id, "fake")
        XCTAssertNotEqual(topic.fields.topic, "topic_fake")
        XCTAssertNotEqual(topic.fields.count, 4)
    }
    
    func testEqual() {
        let topic: Topic = Topic(id: "1", fields: Topic.Fields(topic: "topic", count: 3))
        let topic2: Topic = Topic(id: "1", fields: Topic.Fields(topic: "topic", count: 3))
        XCTAssertEqual(topic, topic2)
    }

}
