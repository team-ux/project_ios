//
//  SponsorModelTest.swift
//  project_iosTests
//
//  Created by Thomas GUILLAUME on 07/04/2021.
//

import XCTest
@testable import project_ios

class SponsorModelTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSponsorInit() {
        let sponsor: Sponsor = Sponsor(id: "123", fields: Sponsor.Fields(company: "a", contacts: ["1","2"], previousSponsor: false, notes: "note", sponsoredAmount: 1000, status: "Received pledged $"))
        XCTAssertEqual(sponsor.id, "123")
        XCTAssertEqual(sponsor.fields.company, "a")
        XCTAssertEqual(sponsor.fields.contacts, ["1","2"])
        XCTAssertEqual(sponsor.fields.previousSponsor, false)
        XCTAssertEqual(sponsor.fields.notes, "note")
        XCTAssertEqual(sponsor.fields.sponsoredAmount, 1000)
        XCTAssertEqual(sponsor.fields.status, "Received pledged $")
    }
    
    func testSponsorWrongInit() {
        let sponsor: Sponsor = Sponsor(id: "123", fields: Sponsor.Fields(company: "a", contacts: ["1","2"], previousSponsor: false, notes: "note", sponsoredAmount: 1000, status: "Received pledged $"))
        XCTAssertNotEqual(sponsor.id, "12")
        XCTAssertNotEqual(sponsor.fields.company, "b")
        XCTAssertNotEqual(sponsor.fields.contacts, ["1","3"])
        XCTAssertNotEqual(sponsor.fields.previousSponsor, true)
        XCTAssertNotEqual(sponsor.fields.notes, "note2")
        XCTAssertNotEqual(sponsor.fields.sponsoredAmount, 104)
        XCTAssertNotEqual(sponsor.fields.status, "Received")
    }
    
    func testTotalReceivedPleged() {
        let sponsor1: Sponsor = Sponsor(id: "1", fields: Sponsor.Fields(company: "a", contacts: ["1","2"], previousSponsor: true, notes: "note", sponsoredAmount: 1000, status: "Received pledged $"))
        let sponsor2: Sponsor = Sponsor(id: "2", fields: Sponsor.Fields(company: "b", contacts: ["1","2"], previousSponsor: false, notes: "note", sponsoredAmount: 1000, status: "Received pledged $"))
        let sponsor3: Sponsor = Sponsor(id: "3", fields: Sponsor.Fields(company: "c", contacts: ["1","2"], previousSponsor: false, notes: "note", sponsoredAmount: 1000, status: "Received pledged $"))
        
        let sponsorRecords = SponsorRecords(records: [sponsor1, sponsor2, sponsor3])
        XCTAssertEqual(sponsorRecords.totalReceivedPleged, 3000)
        XCTAssertNotEqual(sponsorRecords.totalReceivedPleged, 0)
    }
    
    func testTotalPleged() {
        let sponsor1: Sponsor = Sponsor(id: "1", fields: Sponsor.Fields(company: "a", contacts: ["1","2"], previousSponsor: true, notes: "note", sponsoredAmount: 1000, status: "Pledged $"))
        let sponsor2: Sponsor = Sponsor(id: "2", fields: Sponsor.Fields(company: "b", contacts: ["1","2"], previousSponsor: false, notes: "note", sponsoredAmount: 1000, status: "Pledged $"))
        let sponsor3: Sponsor = Sponsor(id: "3", fields: Sponsor.Fields(company: "c", contacts: ["1","2"], previousSponsor: false, notes: "note", sponsoredAmount: 1000, status: "Pledged $"))
        
        let sponsorRecords = SponsorRecords(records: [sponsor1, sponsor2, sponsor3])
        XCTAssertEqual(sponsorRecords.totalPleged, 3000)
        XCTAssertNotEqual(sponsorRecords.totalPleged, 0)

    }

}
