//
//  RequestServiceTests.swift
//  project-iosTests
//
//  Created by Thomas GUILLAUME on 24/03/2021.
//

import XCTest
@testable import project_ios

class RequestServiceTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testCreateRequest() throws {
        let urlStr = "https://api.airtable.com/v0/appXKn0DvuHuLw4DV/Schedule?param1=1&param2=2"
        let r = RequestService()
        let url = URL(string: urlStr)!
        let parameters: [String: String] = ["param1": "1", "param2": "2"]
        let requestURL = r.createRequest(urlStr: "Schedule", type: "GET", parameters: parameters)
        XCTAssertEqual(requestURL.url, url)
        XCTAssertEqual(requestURL.httpMethod, "GET")
        XCTAssertEqual(requestURL.timeoutInterval, 100)
        XCTAssertEqual(requestURL.value(forHTTPHeaderField: "Authorization"),
                       "Bearer \(RequestService.API_KEY)")
    }
    
    func testCreateRequestFailed() throws {
        let urlStr = "https://fake.com"
        let r = RequestService()
        let url = URL(string: urlStr)!
        let requestURL = r.createRequest(type: "POST")
        XCTAssertNotEqual(requestURL.url, url)
        XCTAssertNotEqual(requestURL.httpMethod, "GET")
        XCTAssertNotEqual(requestURL.timeoutInterval, 1000)
        XCTAssertNotEqual(requestURL.value(forHTTPHeaderField: "Authorization"),
                       "Bearer FAKE_API_KEY")
    }

}
