//
//  SpeakerModelTest.swift
//  project_iosTests
//
//  Created by Thomas GUILLAUME on 07/04/2021.
//

import XCTest
@testable import project_ios

class SpeakerModelTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSpeakerInit() {
        let speaker: Speaker = Speaker(id: "2", fields: Speaker.Fields(role: "admin", company: ["apple"], status: "CEO", email: "steve.job@apple.com", type: "Speaker", speakingAt: ["1", "2"], name: "steve job", phone: "0606060606"))
        XCTAssertEqual(speaker.id, "2")
        XCTAssertEqual(speaker.fields.role, "admin")
        XCTAssertEqual(speaker.fields.company, ["apple"])
        XCTAssertEqual(speaker.fields.status, "CEO")
        XCTAssertEqual(speaker.fields.email, "steve.job@apple.com")
        XCTAssertEqual(speaker.fields.type, "Speaker")
        XCTAssertEqual(speaker.fields.speakingAt, ["1", "2"])
        XCTAssertEqual(speaker.fields.name, "steve job")
        XCTAssertEqual(speaker.fields.phone, "0606060606")
    }
    
    func testSpeakerWrongInit() {
        let speaker: Speaker = Speaker(id: "2", fields: Speaker.Fields(role: "admin", company: ["apple"], status: "CEO", email: "steve.job@apple.com", type: "Speaker", speakingAt: ["1", "2"], name: "steve job", phone: "0606060606"))
        XCTAssertNotEqual(speaker.id, "3")
        XCTAssertNotEqual(speaker.fields.role, "admin_fake")
        XCTAssertNotEqual(speaker.fields.company, ["apple_fake"])
        XCTAssertNotEqual(speaker.fields.status, "CEO_fake")
        XCTAssertNotEqual(speaker.fields.email, "steve.job_fake@apple.com")
        XCTAssertNotEqual(speaker.fields.type, "Attendee")
        XCTAssertNotEqual(speaker.fields.speakingAt, ["1", "3"])
        XCTAssertNotEqual(speaker.fields.name, "steve job fake")
        XCTAssertNotEqual(speaker.fields.phone, "0606060606_fake")
    }
    
    func testEqual() {
        let speaker: Speaker = Speaker(id: "2", fields: Speaker.Fields(role: "admin", company: ["apple"], status: "CEO", email: "steve.job@apple.com", type: "Speaker", speakingAt: ["1", "2"], name: "steve job", phone: "0606060606"))
        let speaker2: Speaker = Speaker(id: "2", fields: Speaker.Fields(role: "admin", company: ["apple"], status: "CEO", email: "steve.job@apple.com", type: "Speaker", speakingAt: ["1", "2"], name: "steve job", phone: "0606060606"))
        XCTAssertEqual(speaker, speaker2)
    }

}
