//
//  LocationModelTest.swift
//  project_iosTests
//
//  Created by Thomas GUILLAUME on 07/04/2021.
//

import XCTest
@testable import project_ios

class LocationModelTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLocationInit() {
        let photos: [Location.Photo] = [
            Location.Photo(id: "1", url: "http://photo1.png", filename: "photo1", size: 128, type: "T"),
            Location.Photo(id: "2", url: "http://photo2.png", filename: "photo2", size: 128, type: "T"),
            Location.Photo(id: "3", url: "http://photo3.png", filename: "photo3", size: 128, type: "T")
        ]
        let location: Location = Location(id: "1", fields: Location.Fields(spaceName: "spaceName", buildingLocation: "address", photos: photos, description: "description", maxCapacity: 128, scheduledEvents: ["1", "2"]))
        
        XCTAssertEqual(location.id, "1")
        XCTAssertEqual(location.fields.spaceName, "spaceName")
        XCTAssertEqual(location.fields.buildingLocation, "address")
        XCTAssertEqual(location.fields.description, "description")
        XCTAssertEqual(location.fields.maxCapacity, 128)
        XCTAssertEqual(location.fields.scheduledEvents, ["1", "2"])
        XCTAssertEqual(location.fields.photos, [
            Location.Photo(id: "1", url: "http://photo1.png", filename: "photo1", size: 128, type: "T"),
            Location.Photo(id: "2", url: "http://photo2.png", filename: "photo2", size: 128, type: "T"),
            Location.Photo(id: "3", url: "http://photo3.png", filename: "photo3", size: 128, type: "T")
        ])
    }
    
    func testLocationWrongInit() {
        let photos: [Location.Photo] = [
            Location.Photo(id: "1", url: "http://photo1.png", filename: "photo1", size: 128, type: "T"),
            Location.Photo(id: "2", url: "http://photo2.png", filename: "photo2", size: 128, type: "T"),
            Location.Photo(id: "3", url: "http://photo3.png", filename: "photo3", size: 128, type: "T")
        ]
        let location: Location = Location(id: "1", fields: Location.Fields(spaceName: "spaceName", buildingLocation: "address", photos: photos, description: "description", maxCapacity: 128, scheduledEvents: ["1", "2"]))
        
        XCTAssertNotEqual(location.id, "1_fake")
        XCTAssertNotEqual(location.fields.spaceName, "spaceName_fake")
        XCTAssertNotEqual(location.fields.buildingLocation, "address_fake")
        XCTAssertNotEqual(location.fields.description, "description_fake")
        XCTAssertNotEqual(location.fields.maxCapacity, 0)
        XCTAssertNotEqual(location.fields.scheduledEvents, ["1_fake"])
        XCTAssertNotEqual(location.fields.photos, [
            Location.Photo(id: "1", url: "http://photo1.png", filename: "photo1", size: 128, type: "T"),
        ])
    }
    
    func testEqual() {
        let photos: [Location.Photo] = [
            Location.Photo(id: "1", url: "http://photo1.png", filename: "photo1", size: 128, type: "T"),
            Location.Photo(id: "2", url: "http://photo2.png", filename: "photo2", size: 128, type: "T"),
            Location.Photo(id: "3", url: "http://photo3.png", filename: "photo3", size: 128, type: "T")
        ]
        let location: Location = Location(id: "1", fields: Location.Fields(spaceName: "spaceName", buildingLocation: "address", photos: photos, description: "description", maxCapacity: 128, scheduledEvents: ["1", "2"]))
        let photos2: [Location.Photo] = [
            Location.Photo(id: "1", url: "http://photo1.png", filename: "photo1", size: 128, type: "T"),
            Location.Photo(id: "2", url: "http://photo2.png", filename: "photo2", size: 128, type: "T"),
            Location.Photo(id: "3", url: "http://photo3.png", filename: "photo3", size: 128, type: "T")
        ]
        let location2: Location = Location(id: "1", fields: Location.Fields(spaceName: "spaceName", buildingLocation: "address", photos: photos2, description: "description", maxCapacity: 128, scheduledEvents: ["1", "2"]))
        XCTAssertEqual(location, location2)
    }
    
    func testNotEqual() {
        let photos: [Location.Photo] = [
            Location.Photo(id: "1", url: "http://photo1.png", filename: "photo1", size: 128, type: "T")
        ]
        let location: Location = Location(id: "1", fields: Location.Fields(spaceName: "spaceName", buildingLocation: "address", photos: photos, description: "description", maxCapacity: 128, scheduledEvents: ["1", "2"]))
        let photos2: [Location.Photo] = [
            Location.Photo(id: "1", url: "http://photo1.png", filename: "photo1", size: 128, type: "T"),
            Location.Photo(id: "2", url: "http://photo2.png", filename: "photo2", size: 128, type: "T"),
            Location.Photo(id: "3", url: "http://photo3.png", filename: "photo3", size: 128, type: "T")
        ]
        let location2: Location = Location(id: "1", fields: Location.Fields(spaceName: "spaceName", buildingLocation: "address", photos: photos2, description: "description", maxCapacity: 128, scheduledEvents: ["1", "2"]))
        XCTAssertNotEqual(location, location2)
    }

}
